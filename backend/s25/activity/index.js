console.log("Hello world");


	let trainer = {
		name: "Brock",
		age: 21,
		pokemon: ['Onix','Graveler','Scyther','Steelix'],
		friends: {
			hoenn: ['Professor Oak','Gray'],
			kanto: ['Jessie','James']
		},
		talk: function(){

			return ("Pikachu! I Choose you!")
		}
	}
	console.log(trainer);

	console.log("Result of dot notation:")
	console.log(trainer.name);

	console.log("Result of square bracket notation:")
	console.log(trainer['pokemon']);

	console.log("Result of talk method:")
	console.log(trainer.talk());



 function Pokemon(name,level){
 		this.name = name;
 		this.level = level;
 		this.health = 2 * level;
 		this.attack = 1 * level;

 		this.tackle = function(target){
 			let x = target.health -= this.attack;

 			console.log(this.name + " tackled " + target.name);
 			
 			if (x > 0){

 			 return target.name + "'s health is now reduced to " + x;
 			}
 			else if (x <= 0){
 				return this.faint(target);
 			}
 		}

 		this.faint = function(target){
			return target.name + " fainted."
		}


 }

 let pikachu = new Pokemon("Pikachu",40);
 let psyduck = new Pokemon('Psyduck',100);
console.log(pikachu.tackle(psyduck));
console.log(pikachu.tackle(psyduck));
console.log(pikachu.tackle(psyduck));
console.log(pikachu.tackle(psyduck));
console.log(pikachu.tackle(psyduck));



//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}