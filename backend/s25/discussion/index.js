console.log("JS Objects");

// [OBJECTS]
/*
	- An object is a data type that is used to represent real world objects

	- Information stored in objcts are represented in a "key:value" pair which is separated by comma(,)
	- A "Key" is also mostly referred to as a "property" of an object
	- Different data types may also be stored in an object's proprty creating complex data structures.

	SYNTAX:

		let objectName = {
		
			keyA: valueA,
			keyB: valueB
		}
*/

// Creating objects using OBJECT INITIALIZER/LITERAL notation

let ninja = {
	name: "Naruto",
	village: "Konoha",
	occupation: "Hokage"
}

console.log("Result from creating objects using initializers/literal notation");
console.log(ninja);
console.log(typeof ninja);

let dog = {
	name: "Whitey",
	color: "white",
	breed: "Chihuahua"
}


// Create OBJECTS using a CONSTRUCTOR FUNCTION

/*
	Create a reusable function to create several objects that have the same data structure

	INSTANCES - is a concrete occurence of any object which emphasizes on its distinct/unique identity of it. ex. (laptop1) & (myLaptop) on below codes.

	SYNTAX:

		(variable should start with uppercase - ObjectName)

		function ObjectName(keyA,keyB){
			this.keyA = keyA;
			this.keyB = keyB;
		}

*/

	function Laptop(name,manufactureDate){
		this.name = name;
		this.manufactureDate = manufactureDate;
	}

	/*
		"this" keyword allows us to assign a new object's properties by associating them with the values received from a constructor function's parameter.
	*/

	/*
		"new" operator creates an INSTANCE of an object.

		Object and instances ar often interchanged because object literals (let object = {}) and 
		instances (let object = new Object) 
		are distinct/unique objects
	*/

// INSTANCE #1 
	let laptop1 = new Laptop('Lenovo', 2022)
	console.log('Result from creating objects using object constructor');
	console.log(laptop1);

// INSTANCE #2
	let myLaptop = new Laptop('MacBook Air', 2020);
	console.log('Result from creating objects using object constructor');
	console.log(myLaptop);



// INSTANCE #3
	let oldLaptop = Laptop('Portal R2E CCMC', 1980);
	console.log("Result from creating instance without new keyword");
	console.log(oldLaptop);

	/*
		The result for this INSTANCE is 'undefined' because we invoke/call "Laptop" function instead of creating a new object instance.

		returns undefined without the "new" operator because the "Laptop" function does not have a return statement
	*/


	// Mini-Activity 1 - Create 3 more instances for the Laptop constructor

	let laptop2 = new Laptop('ASUS ROG', 2023);
	console.log('Result from creating objects using object constructor');
	console.log(laptop2);

	let laptop3 = new Laptop('DELL', 2021);
	console.log('Result from creating objects using object constructor');
	console.log(laptop3);

	let laptop4 = new Laptop('MSI', 2022);
	console.log('Result from creating objects using object constructor');
	console.log(laptop4);


	// Create EMPTY OBJECTS - we can initialize empty objects

	let computer = {};
	let myComputer = new Object();
	console.log(computer);
	console.log(myComputer);


// [ACCESS OBJECT PROPERTIES]

/*
	
*/

	// Access name & manufactureDate - myLaptop

	// 1. dot notation
	console.log('Result: ' + myLaptop.name);
	console.log('Result: ' + myLaptop.manufactureDate);

	// 2. square bracket notation
	console.log('Result: ' + myLaptop['name']);
	console.log('Result: ' + myLaptop['manufactureDate']);

// [ACCESS ARRAY OBJECTS]
/*
	Accessing array elements can also be done using square brackets
	Accessing object properties using the square bracket notation, and array indexes can cause confusion

*/	
	// Example

	let array = [laptop1, myLaptop];

	// Square Bracket method
		// may be confused for accessing array indexes
	console.log(array[0]['name']);

	// Dot Notation method
		// differentiation between accessing arrays and object properties
	console.log(array[0].name);

	// This example tells us that array[0] is an object by using the dot notation



// [INITIALIZE, ADD, DELETE, & REASSIGN Object Properties]

	// Initialize empty object 
 let car = {};

 car.name = 'Honda Civic';
 console.log("result from adding properties using dot notation:");
 console.log(car);

 // Example of Array in Empty Object

 // car.number = [1,2,3]
 // console.log(car);


// Square Bracket notation allows space on properties which may result to errors so it is recommended to use dot notation
 car['manufacture date'] = 2019;
 console.log(car['manufacture date']);
 console.log(car);
 console.log(car.manufactureDate);//undefined

 // DELETE Object Properties

 delete car['manufacture date'];
 console.log("Result from deleting properties: ");
 console.log(car);


 // REASSIGN Object Properties

 car.name = 'Dodge Charger R/T';
 console.log("Result from reassigning properties:");
 console.log(car);


// [OBJECT METHODS]
/*
	A method is a function which is a property of an object.

	Similar to functions/features of real world objects, methods are defined based on what an object is capable of doing and how it should work.
*/
	// On the example below - talk is now a METHOD

	let person = {
		name: "Cardo",
		talk: function(){
			console.log('Hello my name is ' + this.name);
		}
	}
	console.log(person);
	console.log('Result of Object Methods:');
	person.talk();

	// add walk in the person function:

	person.walk = function(){
		console.log(this.name + ' walked a 25 steps forward!');
	}
	person.walk();


	let friend = {
		firstName: 'Nami',
		lastName: 'Misko',
		address: {
			city: 'Tokyo',
			country: 'Japan'
		},
		emails: ['nami@sea.com', 'namimisko@gmail.com'],
		introduce: function(){
			console.log('Hello! my name is ' + this.firstName + ' ' + this.lastName);
		}
	}

	friend.introduce();


	// [REAL WORLD APPLICATIONS OF OBJECTS]

	/*
		SCENARIO:
			1. We would like to create a game that would have several pokemon interact with each other
			2. Every pokemon would have the same set of stats, properties and functions
	*/

	// Use OBJECT LITERALS:

	let myPokemon = {
		name: "Pikachu",
		level: 3,
		health: 100,
		attack: 50,
		tackle: function(){
			console.log("This Pokemon tackled Target Pokemon!")
			console.log("Target Pokemon's health is now reduced to Target Pokemon health")
		},
		faint: function(){
			console.log("Pokemon fainted")
		}
	}

	console.log(myPokemon);
	myPokemon.faint();


	// Use OBJECT CONSTRUCTOR:

	function Pokemon(name,level){

		// Properties:
		this.name = name;
		this.level = level;
		this.health = 2 * level;
		this.attack = level;

		// Methods
		this.tackle = function(target){
			console.log(this.name + ' tackle ' + target.name);
			console.log(target.name + "'s health is now reduced to _targetPokemonHealth_");
		}

		this.faint = function(){
			console.log(this.name + ' fainted.');
		}
	}

	let pikachu = new Pokemon("Pikachu",16);
	let rattata = new Pokemon('Rattata',8);
	pikachu.tackle(rattata);


