console.log("Array Manipulation!");

// ARRAY METHODS
	// JS has built-in functions and methods for Arrays. This allows us to manipulate and access Array items.

/*
	MUTATOR METHODS

	- Are functions that mutate or change an array after theu are created.

	- These methods manipulate the original array performing various tasks such as adding and removing elements.

*/

let fruits = ['Apple','Orange','Kiwi','Dragon Fruit'];
console.log(fruits);
console.log(typeof fruits);


/* push() - adds an element in the END of an array AND returns the array's LENGTH.

Syntax:
	arrayName.push("itemToPush");

*/

console.log('Current array: ');
console.log(fruits);

// if we assign it to a variable we are able to save the length of the Array.

let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);
console.log('Mutated array from push method:');
console.log(fruits);

// We can push multiple items at the same time.
// fruits.push('Sample1', 'sample2');
// console.log(fruits);

/*
	pop() - Removes the LAST element in an array AND returs the REMOVED element

	Syntax:
		arrayName.pop()

*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log('Mutated array from POP method:');
console.log(fruits);

// Mini-activity - using POP method in a function

function removeFruit(){
	fruits.pop();
}
removeFruit();
console.log(fruits);


/*
	unshift() - adds 1 or more element at the beginning of an Array.

	Syntax:
		arrayName.unshift('elementA'); - single element
		arrayName.unshift('elementA','elementB') - multiple elements
*/

fruits.unshift('Lime','Banana');
console.log('Mutated array from unshift method:');
console.log(fruits);

function unshiftFruit(fruit1, fruit2){
	fruits.unshift(fruit1, fruit2);
}

unshiftFruit("Grapes","Papaya");
console.log(fruits);


/*
	shift() - removes an element at the beginning of an Array AND returns the REMOVED element.

	Syntax:
		arrayName.shift();
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from the Shift method: ");
console.log(fruits);



/*
	splice() - can REMOVE and ADD more than 1 element in an Array. Simultaneously removes elements from a specified index and also adds elements.

	Syntax:
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);
*/

	fruits.splice(1, 2, 'Calamansi','Cherry');
	console.log('Mutated array from Splice method:');
	console.log(fruits);

	function spliceFruit(index,deleteCount,fruit) {
		fruits.splice(index,deleteCount,fruit);
	}

	spliceFruit(1,1,"Avocado");
	spliceFruit(2,1,"Durian");
	console.log(fruits);

/*
	sort() - this rearranges the Array elements in the alphanumeric order.(A-Z)

	Syntax:
		arrayName.sort()

*/

fruits.sort();
console.log("Mutated array from sort method: ");
console.log(fruits);


/*
	reverse() - this reverses the order of Array elements. (Z-A)

	Syntax:
		arrayName.reverse();
*/

fruits.reverse();
console.log("Mutated array from reverse method:");
console.log(fruits);


