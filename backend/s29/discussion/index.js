console.log("DOM Manipulation!");

// Added interactivity for the clicker:

console.log(document);// html document
console.log(document.querySelector('#clicker'));//button element


/*	document = refers to the whole webpage
	
	querySelector = used to select a specific element (obj) as long as it is inside the HTML tag(html element).
				= takes a string input that if formatted like CSS Selector
				= Can select elements regardless if the string is an id, class, or tag as long as the element is existing in the webpage.
*/

/*
	ALTERNATIVE METHODS that we use aside from querySelector in retrieving elements:

		document.getElementbyId()
		document.getElementByClassName()
		document.getElementByTagName()
*/

let counter = 0;

const clicker = document.querySelector('#clicker');


	clicker.addEventListener('click',()=>{
		console.log("The button has been clicked");
		counter++;
		alert(`The button has been clicked ${counter} times`);
		// The use of `` and ${variable}
	})

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

// Using Arrow Function (=>) 
const updateFullName = (e) =>{
	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;

	spanFullName.innerHTML = `${firstName} ${lastName}`
}

	txtLastName.addEventListener('keyup', updateFullName);
	txtFirstName.addEventListener('keyup', updateFullName);



/*
THE CODE BELOW IS THE OTHER WAYROUND FOR THE CODE ABOVE

txtFirstName.addEventListener('keyup',(event)=>{
	spanFullName.innerHTML = txtFirstName.value
})

txtLastName.addEventListener('keyup',(event)=>{
	spanFullName.innerHTML = (txtFirstName.value + ' ' + txtLastName.value);
})

*/

// The code below will trigger alert when mouse hovers on the First Name label on the webpage.
// const labelFirstName = document.querySelector("#label-txt-first-name");

// labelFirstName.addEventListener('mouseover',(e)=>{

// 	alert("You hovered the First Name Label")
// })

