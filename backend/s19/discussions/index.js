//Syntax, Statements, and Comments

console.log("We tell the computer to log this in the console.");
alert("We tell the computer to display an alert with this message!");

//Statements - in programming are instructions that we tell the computer to perform
//JS Statements usually end with semicolon (;)

//Syntax - it is the set of rules that describes how statements must be constructed

//Comments -For us to create a comment, we use Ctrl + /
//Singline (Ctrl +/) & Multi-line comments (Ctrl + Shift + /)

// console.log("Hello!");

/*alert("This is an alert!");
alert("This is another alert");*/

//Whitespaces (space and line breaks) can impact functionality in many computer languages, but not in JS.

//Variables - this is used to contain data.

//Declare variables
	// tell our devices that a variableName is created and is ready to store data
	//Syntax
		//let/const varaibleName;

		let myVariable;
		console.log(myVariable)

		// Variables must be declared first before they are used.
		let hello;
		console.log(hello);

		//declaring and initializing variables
		//Syntax
			//let/const variableName = initialValue;

		let productName = 'desktop computer';
		console.log(productName);

		let productPrice = 18999;
		console.log(productPrice);

		const interest = 3.559;

		//re-assigning variable values

		productName = 'Laptop';
		console.log(productName)

		/*interest = 3.5;
		console.log(interest);*/

		let friend = 'Kate';
		 friend = 'Jane'

		 //Declare a Variable
		 let supplier;

		 //initialization is done after the variable has been delacred
		 supplier = "John Smith Tradings"

		 //This is considered as re-assignment because its initial value was already declared
		 supplier = "Zuitt Store";


		 // We cannot delare a const variable without initialization.
		/* const pi;
		 pi = 3.1416;
		 console.log(pi);*/

		 //Multivariable variable declatrion
		 let productCode = 'DC017';
		 const productBrand = 'Dell';


		 // Data Types

		 //Strings

		 let country = 'Philippines';
		 let province = "Metro Manila";

		 //Concentenating String
		 let fullAddress = province + ','+ country;
		 console.log(fullAddress)

		 let greeting = 'I live in the ' + country
		 console.log(greeting)

		 //the escape character (\) in strings in combination with other characters can produce different effects

		 //"\n" refers to creating a new line in between text

		 let mailAddress = 'Metro Manila\n\n\n\nPhilippines';
		 console.log(mailAddress);

		 let message = "John's employees went home early";
		 console.log(message);
		 message = 'John\'s employees went home early';
		 console.log(message)

		 //Numbers

		 let headcount = 26
		 console.log(headcount);
		 let grade = 98.7;
		 console.log(grade);
		 let planetDisctance = 2e10;
		 console.log(planetDisctance);

		 //Combine text and strings
		 console.log("John's first grade last quarter is "+ grade);

		 //Boolean - only have 2 values - True or False
		 let isMarried = false;
		 let isGoodConduct = true;
		 console.log(isMarried);

		 console.log("isGoodConduct: " + isGoodConduct);

		 //Arrays

		 //Syntax
		 	//let/const arrayName = [elementA, elementB, ...)

		 let grades = [98.7,92.1,90.7,98.6];
		 console.log(grades);

		 //Objects

		 let myGrades = {
		 	firstgrading: 98.7,
		 	secondgrading: 92.1,
		 	thirdGrading: 90.7,
		 	fourthGrading: 98.6,
		 }
		 console.log(myGrades);

		 let person = {
		 	fullName: 'Juan Dela Cruz',
		 	age: 35,
		 	isMarried: false,
		 	contact: ["+639123456789","8700"],
		 	address:{
		 		houseNumber: '345',
		 		city:'Manila',
		 	}
		 }
		 console.log(person);

		 //typeof Operator
		 console.log (typeof person);