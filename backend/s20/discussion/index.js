console.log("Hello World");

// Arithmetic Operators
// + - * / %

let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition Operator: " + sum)

let difference = x - y;
console.log("Result of subtraction Operator: " + difference)

let product = x * y;
console.log("Result of multiplication Operator: " + product)

let quotient = x / y;
console.log("Result of division Operator: " + quotient)

let remainder = y % x;
console.log("Result of modulo Operator: " + remainder)

let a = 10;
let b = 5;

let remainderA = a % b;
console.log(remainderA);

// Assignment Operator
// Basic Assignment Operator (=)

let assignmentNumber = 8;

// Addition assignment operator (+=)

assignmentNumber = assignmentNumber + 2;
assignmentNumber += 2;
console.log(assignmentNumber);

// subtraction/multiplication/division assignment operator

assignmentNumber -= 2;
console.log("Result of -= : " + assignmentNumber);

assignmentNumber *= 2;
console.log("Result of *= : " + assignmentNumber);

assignmentNumber /= 2;
console.log("Result of /= : " + assignmentNumber);

// Multiple Operators and Parentheses

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

let pemdas = 1 + (2-3) * (4/5);
console.log(pemdas);

// Increment and Decrement
// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

 let z = 1;

 // The value of "z" is added by a value of one before returning the value and storing it in the variable "increment"

 // increase then re-assign

  let increment = ++z;
  console.log("Result of the pre-increment: " + increment);
  console.log("Result of the pre-increment: " + z);
 
// The value of "z" is returned and stored in the variable "increment" then the value of z is increased by one

// re-assign then increase

 increment = z++;
 console.log("Result of the post-increment: " + increment);
 console.log("Result of the post-increment: " + z);



// pre-decrement
// decrease then re-assign
 let decrement = --z;
  console.log("Result of the pre-decrement: " + decrement);
  console.log("Result of the pre-decrement: " + z);


// post decrement
// re-assign then decrease
  decrement = z--;
  console.log("Result of the post-decrement: " + decrement);
  console.log("Result of the post-decrement: " + z);

  // Type Coercion

  /*

  	Type Coercion is the automatic or implicit conversion of values from one data type to another

  */

  let numA = '10';
  let numB = 12;

  let coercion = numA + numB;
  console.log(coercion);
  console.log(typeof coercion);


// non-coercion

  let numC = 16;
  let numD = 14;


  let nonCoercion = numC + numD;
  console.log(nonCoercion);
  console.log(typeof nonCoercion);


// the boolean "true" is associated with the value of 1
  let numE = true + 1;
  console.log(numE);


// the boolean "false" is associated with the value of 0
  let numF = false + 1;
  console.log(numF);

  // Comparison Operators

  let juan = 'juan';

  // Equality Operator (==)

  console.log(1 == 1); //true
  console.log(1 == 2); //false
  console.log(1 == '1'); //true
  console.log(0 == false); //true
  console.log('juan' == 'juan'); //true
  console.log('juan' == juan); //true

   // Inequality Operator (!=)

  console.log(1 != 1); //false
  console.log(1 != 2); //true
  console.log(1 != '1');//false
  console.log(0 != false);//false
  console.log('juan' != 'juan');//false
  console.log('juan' != juan);//false


 // Strict Equality Operator (===)

  console.log(1 === 1); //true
  console.log(1 === 2); //false
  console.log(1 === '1'); //false
  console.log(0 === false); //false
  console.log('juan' === 'juan'); //true
  console.log('juan' === juan); //true

 // Strict Inequality Operator (!=)

  console.log(1 !== 1); //false
  console.log(1 !== 2); //true
  console.log(1 !== '1');//true
  console.log(0 !== false);//true
  console.log('juan' !== 'juan');//false
  console.log('juan' !== juan);//false

  // Relational Operators

  let abc = 50;
  let def = 65;

  let isGreaterThan = abc > def;
  let isLessThan = abc < def;
  let isGTorEqual = abc >= def;
  let isLTorEqual = abc <= def;

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTorEqual);
console.log(isLTorEqual);

	let numStr = "30";
	console.log(abc > numStr); 
	//forced coercion to change the string to a number
	

	console.log(def <= numStr);

	let str = "twenty";
	console.log(def >= str);

  // Logical Operators

  let isLegalAge = true;
  let isRegistered = false;

  // && (AND), || (OR), ! (NOT)


  // && - Return TRUE if all operands are TRUE

  let allRequirementsMet = isLegalAge && isRegistered;
  console.log("Result of AND Operator: " + allRequirementsMet);

  // || - Returns TRUE if ONE of the operans are TRUE

  let someRequirementsMet = isLegalAge || isRegistered;
  console.log("Result of OR Operator: " +someRequirementsMet);

  //  ! = Returns the opposite value

  let someRequirementsNotMet = isRegistered;
  console.log("Result of NOT Operator: " + someRequirementsNotMet);
