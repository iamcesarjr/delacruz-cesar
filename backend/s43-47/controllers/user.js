//Controllers

const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Course = require("../models/Course")


/*
	[Check if the email exists already]

	Steps:
	1. "find" mongoose method to find duplicate items.
	2. "then" method to send a response back to the FE application based on the result of the "find" methd.

*/

module.exports.checkEmailExists = (reqbody) =>{

	return User.find({email:reqbody.email}).then(result=>{

		//find method returns a record if a match is found
		if(result.length > 0){
			return true;
		}
		//No Duplicate Found or the User is not yet registered in the database(db).
		else{
			return false;
		}
	})
}

/*
	[User Registration]

	Steps:
	1. create a new user object
	2. make sure that the password is encrypted
	3. save the new User to the database
*/

module.exports.registerUser = (reqbody)=>{

	let newUser = new User({

		firstName: reqbody.firstName,
		lastName: reqbody.lastName,
		email: reqbody.email,
		mobileNo: reqbody.mobileNo,
		password: bcrypt.hashSync(reqbody.password,10)
	})

	return newUser.save().then((user,error)=>{

		if(error){
			return false
		}else{
			return true
		}
	})

	.catch(err=>err)
}

/*
	[User Authentication]

	Steps:
	1. Check the database if user email exists
	2. Compare the password from the req.body and password stored in the database.
	3. Generate a JWT if the user logged in and return false if not.
*/

module.exports.loginUser = (req,res) =>{

	return User.findOne({email:req.body.email}).then(result=>{
		if(result===null){
			return false
		}else{
			//"compareSync" method is used to compare a non-encrypted password and the encrypted password from the database
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password)

			if(isPasswordCorrect){
				return res.send({access: auth.createAccessToken(result)})
			}
			//"else" password does not match
			else{
				return res.send(false);
			}
		}
	})
	.catch(err=>res.send(err))

}

//Activity S44
module.exports.getProfile = (req,res) =>{

	 return User.findById(req.user.id).then(result =>{
			
			result.password = ""
			return res.send(result)

	})
	.catch(err=>res.send(err)
		
	)
};


/*
	[Enroll user to a class]
	Steps:

	1. Find the document in the database using the user's ID
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Atlas Database
*/
module.exports.enroll = async (req,res) =>{

	//To check in the terminal the user ID and course ID
	console.log(req.user.id);
	console.log(req.body.courseId);

	//Checks if the user is an admin and deny the enrollment
	if(req.user.isAdmin){
		return res.send("Action Forbidden")
	}

	//Creates an "isUserUpdated" variable and returns TRUE upon successful update otherwise returns ERROR
	let isUserUpdated = await User.findById(req.user.id).then(user => {

		//Add the courseId in an object and push that object into the user's "enrollment" array
		let newEnrollment = {
			courseId: req.body.courseId
		}

		//Add the course in the "enrollments" array
		user.enrollments.push(newEnrollment);

		//Save the updated user and return TRUE if successful or the error message if failed.
		return user.save().then(user => true).catch(err => err.message)
	});

	//Checks if there are error in updating the user
	if(isUserUpdated !== true){
		return res.send({ message: isUserUpdated});
	}

	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
		let enrollee = {
			userId: req.user.id
		}
		course.enrollees.push(enrollee)

		return course.save().then(course => true).catch(err => err.message)
	});

	if(isCourseUpdated !== true){
		return res.send({ message: isCourseUpdated});
	}

	//Check if both isUserUpdated & isCourseUpdated were TRUE
	if(isUserUpdated && isCourseUpdated){
		return res.send({message: "Enrolled Successfully!"})
	}
}


//Getting User's enrolled courses
module.exports.getEnrollments = (req,res) =>{
	
	User.findById(req.user.id).then(result =>{
		res.send(result.enrollments)
	})
	.catch (err => res.send(err));
}



// Function to reset the password

module.exports.resetPassword = async (req, res) => {
  try {

  	//Used object destructuring
    const { newPassword } = req.body;
    const { id } = req.user; // Extracting user ID from the authorization header

    // Hashing the new password
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    // Updating the user's password in the database
    await User.findByIdAndUpdate(id, { password: hashedPassword });

    // Sending a success response
    res.status(200).json({ message: 'Password reset successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};



// Controller function to update the user profile
module.exports.updateProfile = async (req, res) => {
  try {
    // Get the user ID from the authenticated token
    const userId = req.user.id;

    // Retrieve the updated profile information from the request body
    const { firstName, lastName, mobileNo } = req.body;

    // Update the user's profile in the database
    const updatedUser = await User.findByIdAndUpdate(
      userId,
      { firstName, lastName, mobileNo },
      { new: true }
    );

    res.json(updatedUser);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Failed to update profile' });
  }
}


// Controller function to update a user to admin
module.exports.updateToAdmin = async (req, res) => {
  const { userId } = req.body;

  try {
    // Check if the authenticated user is an admin
    if (!req.user.isAdmin) {
      return res.status(403).json({ message: 'Access denied. Only admin users can perform this action.' });
    }

    // Find the user by userId
    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).json({ message: 'User not found.' });
    }

    // Update the user's role to admin
    user.isAdmin = true;
    await user.save();

    res.status(200).json({ message: 'User updated to admin successfully.' });
  } catch (error) {
    res.status(500).json({ message: 'An error occurred while updating user to admin.' });
  }
};


// Controller function to update enrollment status of a user for a course
module.exports.updateEnrollmentStatus = async (req, res) => {
  const { userId, courseId, status } = req.body;

  try {
    // Check if the authenticated user is an admin
    if (!req.user.isAdmin) {
      return res.status(403).json({ message: 'Access denied. Only admin users can perform this action.' });
    }

    // Find the user by userId
    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).json({ message: 'User not found.' });
    }

    // Find the enrollment for the specific courseId in the user's enrollments
    const enrollment = user.enrollments.find(enrollment => enrollment.courseId === courseId);

    if (!enrollment) {
      return res.status(404).json({ message: 'User enrollment not found.' });
    }

    // Update the user's enrollment status
    enrollment.status = status;
    await user.save();

    res.status(200).json({ message: 'User enrollment status updated successfully.' });
  } catch (error) {
    res.status(500).json({ message: 'An error occurred while updating user enrollment status.' });
  }
};