//Create a Simple Express JS Application

//Dependencies and modules

const express = require("express");
const mongoose = require("mongoose");
//"cors" allows our back-end application to be available to our front-end application
const cors = require("cors");

const userRoutes = require("./routes/user")
const courseRoutes = require("./routes/course")


//Environment Setup
const port = 4000;

//Server Setup
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());



//Database Connection
mongoose.connect("mongodb+srv://iamcesarjr:admin123@batch-297.ebua14s.mongodb.net/S43-S47?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
)

	//PROMPT
	let db = mongoose.connection;
	db.on('error', console.error.bind(console,'Connection error'));
	db.once('open', () => console.log('Connected to MongoDB Atlas.'));

	//[Back-end Routes]
	//http://localhost:4000/users
	app.use("/users",userRoutes)
	app.use("/courses",courseRoutes)

//Server Gateway Response
if(require.main === module){
	app.listen(process.env.PORT || port, ()=>{
		console.log(`API is now online on port ${process.env.PORT || port}`)
	})
}

module.exports = {app,mongoose}