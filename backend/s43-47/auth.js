const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";

/*
	[JSON Web Tokens]
	- is a way of securely passing of information from the server to the front-end or to other parts of the server.

	[TOKEN CREATION]

	Analogy: Pack the gift and provide a lock with the secret code as the key
*/

module.exports.createAccessToken = (user)=>{
	//The data from the user wil be received through forms/req.body.
	//When the user logs in, a TOKEN will be created with the user's information

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	return jwt.sign(data, secret, {});
}

/*
	[TOKEN VERIFICATION]

	Analogy: Receive the gift and open the lock to verify if the sender is legitimate and the gift was not tampered with.
*/

module.exports.verify = (req,res,next)=>{
	//Middlewares which have access to req, res, and next also can send responses to our client.
	//"next" is a callback to pass control to the next middleware or router

	//"token" is retrieved from the request header
	//this can be provided n postman under "Authorization" > "Bearer Tokern"

	//"req.headers.authorization" contains sensitive data and especially our token

	console.log("This is from the req.headers.authorization")
	console.log(req.headers.authorization)
	let token = req.headers.authorization

	if(typeof token === "undefined"){
		return res.send({auth:"Failed. No Token"});
	}else{
		/*
			.slice() - is a method which can be used on strings and arrays.
			slice(<startingPosition>, <endPosition>)

			Bearer: dfshoasad1223dadld
		*/
		console.log("With Bearer prefix")
		console.log(token);
		token = token.slice(7,token.length);
		console.log("No Bearer prefix")
		console.log(token);

		/*
			[Token Decryption]

			Analogy: Open the gift and get the content

		*/

		jwt.verify(token, secret, function(err,decodedToken){

			//validate the token using the verify method decrypting the token using the secret code
			//err will contain the error from decoding your toen. This will contain the reason why we will reject the token.
			if(err){
				return res.send({
					auth:"Failed",
					message: err.message
				})
			}else{
				console.log("Data that will be assigned to the req.user")
				console.log(decodedToken);
				req.user = decodedToken
				next()
				//Middleware function which will let us proceed to the next middleware or controller
			}
		})
	}
}

//Admin Verification
module.exports.verifyAdmin = (req,res,next)=>{

	//verifyAdmin comes AFTER the verified middleware

	if(req.user.isAdmin){
		next()
	}else{
		return res.send({
			auth:"Failed",
			message:"Action Forbidden"
		})
	}
}