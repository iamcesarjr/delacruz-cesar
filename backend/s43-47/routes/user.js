//Routes

//Dependencies and Modules

const express = require("express");
const userController = require("../controllers/user");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

//Routing Component

const router = express.Router();

//Route- POST route

router.post("/checkEmail",(req,res)=>{
	userController.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController))
})

//Register a User
router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController))
})

//User Authentication

router.post("/login",userController.loginUser)

//ACTIVITY s44
router.post("/details",verify, userController.getProfile)

//Route to enroll User to a Course
router.post("/enroll", verify,userController.enroll)

//ACTIVITY S46
router.get("/getEnrollments", verify,userController.getEnrollments)

// POST route for resetting the password
router.post('/reset-password', verify, userController.resetPassword);

// Define the route for updating a user to admin
router.post('/updateAdmin', verify, verifyAdmin, userController.updateToAdmin);


// Define the route for updating enrollment status of a user for a course
router.post('/updateStatus', verify, verifyAdmin, userController.updateEnrollmentStatus);


// Export Route System

module.exports = router;