
const express = require("express");
const courseController = require("../controllers/course");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;


//Routing Component
const router = express.Router();


//Create a Course
router.post("/",verify,verifyAdmin,courseController.addCourse)

//Route for retrieving all Courses
router.get("/all", courseController.getAllCourses);

//Mini-activity
router.get("/",courseController.getAllActiveCourses);

//Route to retrieve a specific course
router.get("/:courseId",courseController.getCourse);

//Edit a specific course
router.put("/:courseId", verify,verifyAdmin, courseController.updateCourse);

router.put("/:courseId/archive", verify,verifyAdmin, courseController.archiveCourse);

router.put("/:courseId/activate", verify,verifyAdmin, courseController.activateCourse);

// Route to search for courses by course name
router.post("/search", courseController.searchCoursesByName);

//Route to search the enrolled users to a specific course (Admin Account)
router.get("/:courseId/enrolled-users", courseController.getEmailsOfEnrolledUsers);

// Define the route for searching courses by price range
router.post("/searchPrice", courseController.searchCoursesByPrice);


module.exports = router;