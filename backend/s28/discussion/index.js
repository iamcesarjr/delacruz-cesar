console.log("Array Non-Mutator and Iterator Methods");

/*
	NON-MUTATOR METHODS
		- are functions that do not modify or change an array after they are created.
		- do not manipulate the original array performing various tasks such as:
			- returning elements from an array
			- combining arrays
			- printing the output

*/

let countries = ['US','PH','CAN','SG','TH','PH','FR','DE'];

/*	
	indexOf()
		- Returns the index number of the first matching element found in an array
		-If no match is found, the result will be -1
*/

let firstIndex = countries.indexOf('PH');
console.log('Result of indexOf method: ' + firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log('Result of indexOf method: ' + invalidCountry);



/*
	lastIndexOf()
		- Returns the index number fo the last matching element found in the Array
*/

 let lastIndex = countries.lastIndexOf('PH');
 console.log('Result of lastIndexOf Method: ' + lastIndex);

 let lastIndexStart = countries.lastIndexOf('PH', 6);// Output: 5
 // let lastIndexStart = countries.lastIndexOf('PH', 3) Output: 1
 console.log('Result of lastIndexOf Method: ' + lastIndexStart);

 /*
	Slice()
		- Portions or Slices elements from an Array AND returns a new array
 */

  console.log(countries);

   let slicedArrayA = countries.slice(2);
   console.log('Result from slice method:');
   console.log(slicedArrayA);
   //Output = ['CAN', 'SG', 'TH', 'PH', 'FR', 'DE'] - Sliced array from index#2 onwards

   let slicedArrayB = countries.slice(2,4);
   console.log('Result from slice method:');
   console.log(slicedArrayB);
   //Output = ['CAN', 'SG'] - Meaning index#2 start of slice until index#4

   let slicedArrayC = countries.slice(-3);
   console.log('Result from slice method:');
   console.log(slicedArrayC);
   //Output = ['PH', 'FR', 'DE'] - with -3, sliced the last 3 elements


   /*
		toString()
			- Returns an array as a string separated by commas
   */

   let stringArray = countries.toString();
   console.log('Result from toString method: ');
   console.log(stringArray);

   let sampArr = [1,2,3];
   console.log(sampArr.toString());


   /*
		concat()
			- combines two arrays and returns the combined result
   */

   let taskArrayA = ['drink HTML','eat Javascript'];
   let taskArrayB = ['inhale CSS','breathe MongoDB'];
   let taskArrayC = ['get Git','be Node'];

   let tasks = taskArrayA.concat(taskArrayB);
   console.log('Result from concat method:');
   console.log(tasks);
   // Output = ['drink HTML', 'eat Javascript', 'inhale CSS', 'Ebreathe MongoDB']

   console.log('Result from concat method:');
   let allTasks = taskArrayA.concat(taskArrayB,taskArrayC);
   console.log(allTasks);
   // Output = ['drink HTML', 'eat Javascript', 'inhale CSS', 'breathe MongoDB', 'get Git', 'be Node']

   let combinedTasks = taskArrayA.concat('smell express','throw react');
    console.log('Result from concat method:');
    console.log(combinedTasks);


   /*
		join()
			- Returns an Array as a string separated by specified separator(string data type) - comma (,) separator by default
   */

   let users = ['John','Jane','Joe','James'];
   console.log(users.join())// Output = John,Jane,Joe,James
   console.log(users.join(''));//Output = JohnJaneJoeJames
   console.log(users.join(' - '));//Output = John - Jane - Joe - James


   /*
		Iteration Methods
		 	- These are loops designed to perform repititive tasks on arrays.
		 	- Array Iteration methods normally work with a function supplied as an argument.
   */

   /*
		forEach()
			 - Similar to for loop that iterates on each array element
			 - for each item in the array, the ANONYMOUS FUNCTION passed in the forEach() method will be run.
			 - The anonymous function is able to receive the current item being iterated or loop over by assigning a PARAMETER.
			 - Variable names for arrays are normally written in the plural form
			 - It is a common practice to use singular form of the array content for the parameter names used in array loops.
			 - forEach() does not return anything
	
   */

   console.log(allTasks);

   	allTasks.forEach(function(task){
   		console.log(task);
   	});


// Example 2
   	let filteredTasks = [];

   	allTasks.forEach(function(task){
   		
   		// This condition, means all array element with characters more than 10 will be pushed to filteredTasks
   		if(task.length > 10){

   			filteredTasks.push(task);
   		}
   	});

   	console.log("Result of filteredTasks:")
   	console.log(filteredTasks);
   	// Output = ['eat Javascript', 'breathe MongoDB']


/*
	map()
		- Iterates on each element AND returns new array with different values depending on the result of the function's operation.
		- We require the use of a "RETURN"
*/

	let numbers = [ 1, 2, 3, 4, 5];

	let numberMap = numbers.map(function(number){

		// This condition will square the number in the array
		return number * number;
	})

	console.log('Original Array:');
	console.log(numbers);
	console.log('Result of Map Method:');
	console.log(numberMap);


// map() vs. forEach()

	let numberForEach = numbers.forEach(function(number){
		return number * number
	})

	console.log(numberForEach);//Output = undefined since forEach do not return anything


/*
	every()
		- Checks if all elements in an Array meet a given condition
		
*/ 

	let allValid = numbers.every(function(number){
		return (number < 3);
		// return (are all numbers less than 3?) = output is FALSE
	})

	console.log("Result of every method:");
	console.log(allValid);//Output = false


/*
	some()
		- Checks if at least one element in the array meets the given condition
*/

	let someValid = numbers.some(function(number){
		return (number < 2);
		// return (are some numbers less than 2?)
	})

	console.log("Result of some method:");
	console.log(someValid);//Output = true


/*
	filter()
		- Returns a new array that contains elements which meets the given condition
		- Returns an empty array if no elements were found
*/

	let filterValid = numbers.filter(function(number){
		return (number < 3);
		// return numbers that are less than 3
	})

	console.log("Result of filter method:")
	console.log(filterValid);


	let nothingFound = numbers.filter(function(number){
		return (number = 0);
		// return numbers that are equal to zero
	})

	console.log("Result of filter method:")
	console.log(nothingFound);


	// Mini-activity = Filtering using forEach

	let filteredNumbers = []

	numbers.forEach(function(number){
		if (number < 3){
			filteredNumbers.push(number);
		}
	})

	console.log("result of forEach method:")
	console.log(filteredNumbers);


	/*
		includes()
			- checks if the argument passed can be found in the array
			- boolean output
	*/

	let products = ["Mouse","Keyboard","Laptop","Monitor"];


	let productFound1 = products.includes("Mouse");
	console.log(productFound1);//Output = true

	let productFound2 = products.includes("Headset");
	console.log(productFound2);//Output = false


	/*
		METHODS can be CHAINED using them one after another
	*/

	let filteredProducts = products.filter(function(product){

		return product.toLowerCase().includes('a');
	})

	console.log(filteredProducts);



/*
	reduce()
		- evaluates elements from left to right and returns/reduces the array into a single value

		ACCUMULATOR PARAMETER - in the function stores the result for every iteration of the loop.

		CURRENT VALUES - is the current element in the array that is evaluated in each iteration of the loop.

		PROCESS:

		1. first element in the array is stored in the ACCUMULATOR
		2. second element in the array is stored in the current value
		3. an operation is performed on the 2 elements
		4. the loop repeats the whole step 1 - 3 until all elements have been worked on

*/

console.log(numbers)//[1,2,3,4,5]
					//1 + 2 = 3
					//3 + 3 = 6
					//6 + 4 = 10
					//10 + 5 = 15

let iteration = 0;

let reducedArray = numbers.reduce(function(x,y){

	//Use to track the current iteration count and accumulator/current value data
	console.warn('Current iteration: ' +  ++iteration);
	console.log('Accumulator: ' + x);
	console.log('Current Value: '+ y);

	return x + y;
})

console.log("Result of reduce method: " + reducedArray)



// REDUCE STRING ARRAYS TO STRING

let list = ['Hello',' From ',' The ',' Other ',' Side'];
let reducedJoin = list.reduce(function(x,y){
	console.log(x);
	console.log(y);
	return x + ' ' + y
})

console.log("Result of reduce method: " + reducedJoin)
