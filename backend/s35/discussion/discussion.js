// MongoDB Query Operators and Field Projection

/*
    [COMPARISON QUERY OPERATORS]

    $gt [Greater than Operator]
    - Allows us to find the documents that have field values greater than or equal to a specified value:
    Syntax:
        db.collectionName.find({field:{$gt:value}})

    $gte [Greater than or equal to Operator]
     Syntax:
        db.collectionName.find({field:{$gte:value}})


    $lt [Less than Operator]
    - Allows us to find the documents that have field values less than or equal to a specified value:
    Syntax:
        db.collectionName.find({field:{$lt:value}})

    $lte [Less than or equal to Operator]
    Syntax:
        db.collectionName.find({field:{$lte:value}})

*/

// example: (Greater than & Greater than or equal to)
    db.users.find({age:{$gt:50}})
    db.users.find({age:{$gte:50}})

// example: (Less than & Less than or equal to)
    db.users.find({age:{$lt:50}})
    db.users.find({age:{$lte:50}})


/*
    [NOT EQUAL OPERATOR]

    $ne [Not Equal Operator]
    - Allows us to find documents that have field NUMBER values not equal to a specified value:
    Syntax:
        db.collectionName.find({field:{$ne:value}})
*/

// example: (Not Equal to)
    db.users.find({age:{$ne:65}})
    db.users.find({age:{$ne:82}})

/*
    $in [In Operator]
    - Allws is to find documents with specific match criteria in one field using different values
    Syntax:
        db.collectionName.find({field: {$in:value}})

*/
    db.users.find({lastName: {$in:["Hawking","Doe"]}})
    db.users.find({courses:{$in:["HTML","React"]}})


/*
    [Logical Query Operators]

    $or [OR Operator]
    - Allows us to find documents that match a single criteria from multiple provided search criteria
    Syntax:
        db.collectionName.find({$or:[{fieldA:valueA},{fieldB:valueB}]})

*/

// Example - OR Operator
    db.users.find({$or:[{firstName:"Neil"},{age:25}]})
    db.users.find({$or:[{firstName:"Neil"},{age:{$gt:30}}]})


/*
    $and [AND Operator]
    - Allows ti find documents matching multiple criteria in a single filed.
    Syntax:
        db.collectionName.find({$and:[{fieldA:valueA0},{filedB:valueB}]})
*/
// Example - AND Operator
    db.users.find({$and:[{age:{$ne:82}},{age:{$ne:76}}]})

/*
    [FIELD PROJECTION]

    Retrieving documents are common operations that we do and by default MongoDB queries return the whole document as a response.

    We can now include or exclue fields from the response


    [INCLUSION]
     - Allows us to include/add specific fields only when retrieving documents.
     - The value provided is 1 to denote that the filed is being included:
     Syntax:
        db.collectionName.find({criteria},{field:1})
*/
    db.users.find(
        {firstName: "Jane"},
        {
            firstName: 1,
            lastName: 1,
            contact:1
        }
    )

/*
    [EXCLUSION]
     - Allows us to exclude/remove specific fields only when retrieving documents
     - The value provided is 0 to denote that the field is being excluded
     Syntax:
        db.collectionName.find({criteria},{field:0})
*/
    db.users.find(
        {firstName: "Jane"},
        {
            contact:0,
            department:0
        }
    )

/*
    [SUPPRESING THE ID FIELD]
    - Allows us to exclude the "_id" when retrieving documents.
    - When using the field projection, field inclusion and exclusion may not be used at the same time.
    - Excluding the "_id" fiels is the only exception to this rule
    Syntax:
        db.collectionName.find({criteria},{_id:0})
*/

    db.users.find(
        {
            firstName:"Jane"
        },
        {
            firstName:1,
            lastName:1,
            contact:1,
            _id:0
        }
    )

/*
    [EVALUATION QUERY OPERATOR]

        $regex Operator
        - Allows us to find documents that match a specific string pattern using regular expressions

        Syntax: 
            db.collectionName.find({field:{$regex:'pattern',$options:'optionValue'}})

        [$option is optional values or not necessary to be included on if needed]
*/

// CASE SENSITIVE QUERY
    db.users.find({firstName: {$regex:'N'}})

// CASE INSENSITIVE QUERY
    db.users.find({firstName: {$regex: 'j', $option: 'i'}})