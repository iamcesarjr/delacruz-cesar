console.log("Array Traversal!")



let task1 = "Brush Teeth";
let task2 = "Eat Breakfast";

let hobbies = ["Play League","Read a book","Listen to music","Code"];
console.log(typeof hobbies);

/*	Arrays are actually special type of object.
	Does it have Key Value pairs?
	YES - index number : element

	Arrays make it easy to manage and manipulate a set of data.

	Method is another term for functions associated with an object and is used to execute statements that are relevant to a specific object.

	SYNTAX:

	 let/const arrayName = [elementA, elementB, elementC...]
*/

// common examples of array:

 let grades = [98.5, 94.6, 90.8, 88.9];
 let computerBrands = ['Acer','Acer','Lenovo','HP','Neo','RedFox','Gateway','Toshiba','Fujitsu'];

 let mixedArr = [12, 'Asus',null,undefined,{}];
 let arraySample = ['Cardo','One Punch Man',25000,false];


console.log(grades);
console.log(computerBrands);
console.log(mixedArr);
console.log(arraySample);

// Mini-activity

let tasks = ["Watching Anime","Jogging","Reading Manga","Foodtrip"];
let capitalCities = [
		"Tokyo",
		"Kuala Lumpur",
		"Singapore City",
		"Bangkok"
];

console.log(tasks);
console.log(capitalCities);

// Alternative way to write arrays

let myTasks = [

	'drink HTML',
	'eat javascript',
	'inhale css',
	'bake react'

]

// Create an array with values from variables:
	let username1 = 'gdragon22';
	let username2 = 'gdino21';
	let username3 = 'ladiesman217';
	let username4 = 'transformers45';
	let username5 = 'noobmaster68';
	let username6 = 'gbutiki78';

	let guildMembers = [username1,username2,username3,username4,username5,username6];
	console.log(guildMembers);
	console.log(myTasks);

/*	
	[.length property]
	- allows us to get and set the total number of items or elements in an array
	- this gives a number data type
	- Can also be used with string

*/

console.log(myTasks.length);//Output - 4
console.log(capitalCities.length);//Output - 4


// Blank Array

let blankArr = [];
console.log(blankArr.length);//Output - 0

// .length with strings

let fullName = "Cardo Dalisay";
console.log(fullName.length);//Output - 13


// Length Property can also set the total number of items in an array, meaning we can actually delete the last item in the array or shorten the array by simply updating the length property of an array.

myTasks.length = myTasks.length - 1;
console.log(myTasks.length);

// To DELETE an specific item in an array, we can employ ARRAY METHODS

// Another example using DECREMENTATION

capitalCities.length--;
console.log(capitalCities);

// Can we do the same with a string?
fullName.length = fullName.length - 1;
console.log(fullName.length);

// Increasing the length of Arrays

let theBeatles = ["John","Paul","Ringo","George"];
theBeatles.length++;
console.log(theBeatles);

// To add elements on empty array | index should be (last index - 1) 
theBeatles[4] = fullName;
console.log(theBeatles);

// To add another member:
theBeatles[theBeatles.length] = "Tanggol";
console.log(theBeatles);


// [Read from Arrays]
/*
	Access elements with the use of their index

	SYNTAX:

		arrayName[index]

*/

console.log(capitalCities[0]);//Accessed the 1st element of the array

console.log(grades[100]);//undefined

let lakersLegends = ["Kobe","Shaq","Lebron","Magic","Kareem"];

console.log(lakersLegends[2]);//Accessing the 3rd elements - Lebron
console.log(lakersLegends[4]);//Accessing the 5th elements - Kareem


// You can save array items in another variables
let currentLaker = lakersLegends[2];
console.log(currentLaker);

console.log("Array before assignment");
console.log(lakersLegends);
lakersLegends[2] = "Pau Gasol";
console.log("Array after re-assignment");
console.log(lakersLegends);


// Mini-activity 2

let favoriteFoods = [

	"Tonkatsu",
	"Adobo",
	"Pizza",
	"Lasagna",
	"Sinigang"
];

favoriteFoods[3] = "Buffalo Wings";
favoriteFoods[4] = "Carbonara";
console.log(favoriteFoods);


// Mini-activity 3 - find blackmamba from Array using function

 function findBlackMamba(i){
 	return lakersLegends[i]

 }
let blackMamba = findBlackMamba(0);
console.log(blackMamba);



// Mini-activity 4 - Add trainers using functions
let theTrainers = ['Ash'];

 function addTrainers(trainer){
 	theTrainers[theTrainers.length] = trainer;
 }

addTrainers("Misty");
addTrainers("Brock");
console.log(theTrainers);


// Access the last element of an array
// Since the FIRST element of an array starts at 0, subtracting 1 to the 1 length of the array will offset the value by one allowing us to get the last element.

let bullsLegends = ["Jordan","Pippen","Rodman","Rose","Kukoc"];
let lastElementIndex = bullsLegends.length - 1;

// Method 1
console.log(bullsLegends[lastElementIndex]);

// Method 2
console.log(bullsLegends[bullsLegends.length - 1]);


// ADD items into the Array
// We can re-assign EMPTY ARRAY

let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);

newArr[1] = "Tifa Lockhart";
console.log(newArr);

newArr[newArr.length] = "Barrett Wallace";
console.log(newArr);

// To change the value on the last element
newArr[newArr.length - 1] = "Aerith Gainsborough";
console.log(newArr);


// [LOOP OVER AN ARRAY]

for(let index = 0; index < newArr.length; index++){

	console.log(newArr[index])
}


let numArr = [5,12,30,48,40];

for(let index = 0; index < numArr.length; index++){

	// IF STATEMENT for us to know that number is divisible by 5
	if(numArr[index] % 5 === 0){

		console.log(numArr[index] + " is divisible by 5!")

	}else{
		console.log(numArr[index] + " is not divisible by 5!")

	}

}


// [MULTI-DIMENSIONAL ARRAYS] - Arrays within an Array
//  - useful for complex data array

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
]; 

console.log(chessBoard);

// TO Access elements of a MultiDimension Array

console.log(chessBoard[1][4])// Output : e2
console.log("Pawn moves to: " + chessBoard[1][5]);

console.log(chessBoard[7][0])// Output : a8
console.log(chessBoard[5][7])// Output : h6
console.log(chessBoard[0][0])// Output : a1

