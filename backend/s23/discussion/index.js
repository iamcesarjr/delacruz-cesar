console.log("Hello, B297!")

// [If, else if, and else statements]

let numG = -1;


// IF statement:
// executes a astatement if a specified condition is true

 if(numG < 0){
 	console.log("Hello! The condition in the if statement is true!")
 };

let numH = 1

 // ELSE IF statement
 // execute a statement if the previous condition is false and if the specified condition is true
 // the Else If clause is OPTIONAL and can be added to capture additional conditions to change the flow of a program!

if(numG > 0){
	console.log("Hello!")
}

else if(numH > 0){
	console.log("This will log if the else if condition is true and the condition if condition is true!")
}

// ELSE statement
// executes a statement if all other conditions are false
// the "ELSE" statement is OPTIONAL and can be added to capture any other result to change the flow of our program
 
if(numG > 0){
	console.log("I'm enchanted to meet you!")
}

else if(numJ = 0){
	console.log("It's me, Hi...")
}
else {
	console.log("Hello from the other side! This will log of the if and else-if conditions are not met!")
}

// [IF, ELSE IF, and ELSE statements with functions]

let message = 'No message!';
console.log(message);

	function determineTyphoonIntensity(windSpeed){

		if(windSpeed < 30){
			return 'Not a typhoon yet!'
		}
	
		else if(windSpeed <= 61){
			return 'Tropical Depresison detected'
		}

		else if (windSpeed >= 62 && windSpeed <= 88){
			return 'Tropical Storm Detected!'
		}
		else if(windSpeed >= 89 && windSpeed <= 117){
			return 'Severe Tropical Storm detected!'
		}
		else {
			return 'Typhoon detected!'
		}
	}

	message = determineTyphoonIntensity(65);
	// console.log(message);

	// console.war() is a good way t print warnings in our console that could help us devs act on certain output within our code

	if(message == 'Tropical Storm detected!'){
		console.warn(message);
	}

	// [Truthy and Falsy]
	/*
		-In JS a "truthy" value is a value taht is considered true when encountered in a Boolean Context
		- Values are considered true unless defined false

		- Falsy values/exceptions for truthy:
			1. false
			2. 0
			3. -0
			4. ""
			5. null
			6. undefined
			7. NaN

	*/

	// Truthy Examples

	if (true){
		console.log('This is truthy!')
	}

	if (1){
		console.log('This is truthy!')
	}
	if ([]){
		console.log('This is truthy!')
	}

	// Falsy Examples

	if (false){
		console.log('This will not log in the console')
	}

	if (0){
		console.log('This will not log in the console')
	}

	if (undefined){
		console.log('This will not log in the console')
	}

	if ("Cesar"){
		console.log("This will log in the console")
	}


	// [Conditional (Ternary) Operator]

	/*
		Takes in three operands:
		1. Condition
		2. Expresison to execute if the condition is Truthy
		3. Expression to execute if the condition is Falsy

		Commonly used for single statement execution where the result consist

		Syntax: (Ternary Operator)
			(condition) ? ifTrue : ifFalse

	*/

	// single statement execution (Ternary Operator)
	let ternaryResult = (1 < 18) ? true : false;
	console.log("Result of ternary operator: " + ternaryResult);

	// mulitiple statement execution

	let name;

	function isOfLegalAge(){
		name = 'John';
		return 'You are of the legal age limit!'
	}

	function isUnderAge(){
		name = "Jane";
		return 'You are under the age limit!'
	}

	// let age = parseInt(prompt("What is year age?"));
	// console.log(age);

	// let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
	// console.log("Result of Ternary operator in Functions: " + legalAge + ',' + name);


	// [Switch Statement]
		// can be used as an alternative to an IF, ELSE IF, and ELSE statement where the data to be used in the condition is of an EXPECTED input

		/*
			Syntax (Switch Statement)
				switch (expression){
					
					case value:
						statement;
						break;
					default:
						statement;
						break;
				}

		*/
			// Add "toLowerCase to make every values entered be converted to lower case"

		// let day = prompt("What day of the week is it today?").toLowerCase();
		// console.log(day);

		// 	switch (day) {

		// 		case 'monday':
		// 			console.log("The color of the day is blue!")
		// 			break;
		// 		case 'tuesday':
		// 			console.log("The color of the day is yellow!")
		// 			break;
		// 		case 'wednesday':
		// 			console.log("The color of the day is red!")
		// 			break;

		// 		// Mini-acitivity
		// 		case 'thursday':
		// 			console.log("The color of the day is green!")
		// 			break;
		// 			case 'friday':
		// 			console.log("The color of the day is black!")
		// 			break;
		// 			case 'saturday':
		// 			console.log("The color of the day is violet!")
		// 			break;
		// 			case 'sunday':
		// 			console.log("The color of the day is brown!")
		// 			break;

		// 			default:
		// 				console.log("Please input a valid day");
		// 				break;

		// 	}

	// [Try-Catch-Finally Statemen]
	// Are commonly used for error handling

	function showIntensityAlert(windSpeed){

		try {
			// attempt to execute a code
			alerat(determineTyphoonIntensity(windSpeed));
		}

		catch (error){

			console.log(typeof error);
			// "error.message is used to access the information relating to the error object"
			console.warn(error.message);
		}
		finally {
			alert('Intensity updates will show new alert')
		}

	}

	showIntensityAlert(56);
	console.log("Hi!")