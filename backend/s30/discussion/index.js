console.log("ES6 Updates!");

/*
	ES6 Updates

		- is one of the latest versions of writing JS and in fact ONE of the MAJOR updates.

	LET and CONS
		- are ES6 updates, these are the new standards of creating variables.

	HOISTING
		- In JS, allows us to use functions and variables before they are declared. BUT this might cause confusion, because of the confusion that 'var' hoisting can create, it is BEST to AVOID using variables before they are declared.

*/
	console.log(varSample);
	var varSample = "Hoist me up!";
	// The output of the codes above is UNDEFINED

	// if you have used nameVar in other parts of the code, you might be surprised at the output we might get.
	var nameVar = "Camille";

	if(true){
		var nameVar = "Hi!"
	}

	var nameVar = "C";

	console.log(nameVar);


// Examples for let & cons

	let name1 = "Cee";

	if(true){
		let name1 = "Hello";
	}

	// let name1 = "Hello World!"; OUTPUT = error since the variable has already been declared

	console.log(name1);


// [** Exponent Operator] - UPDATE:

	const firstNum = 8**2;
	console.log(firstNum);

	// Other update for exponentation: Math.pow(number,exponent)
	const secondNum = Math.pow(8,2);
	console.log(secondNum);

	let string1 = 'fun';
	let string2 = 'Bootcamp';
	let string3 = 'Coding'
	let string4 = 'JavaScript';
	let string5 = 'Zuitt';
	let string6 = 'love';
	let string7 = 'Learning';
	let string8 = 'I';
	let string9 = 'is';
	let string10 = 'in';

	// MA1 - concatinate strings

		// 1st solution:
		let concatSentence = string8 + " " + string6;

		// 2nd solution:
		let concatSentence1 = string1.concat(" ",string2," ",string3," ",string4," ",string5," ",string6," ",string7," ",string8," ",string9," ",string10,".");
			console.log(concatSentence1);

		/*
		3rd solution: MOST RECOMMENDED
			Template literals [`${ }`] - much efficient to embed statements and greatly helps with code readibility

			${} - is a placeholder that is used to embed JS expresions when creating strings using template literals

		*/

		let concatSentence2 = `${string8} ${string6} ${string5} ${string3} ${string2}`;
		console.log(concatSentence2);


		let name = "Carding";

		let message = `Hello ${name}! Welcome to programming!`;
		console.log(`Message with template literals: ${message}`);


		// Multi-lines using TEMPLATE LITERALS
		const anotherMessage = `
			${name} attended a math competition.

			He won it by solving the problem 8**2 with the solution of ${firstNum}!

		`
		console.log(anotherMessage);


	// Adding operators with placeholders ${}
		const interestRate = .1;
		const principal = 1000;
		console.log(`The interest on your savings account is : ${principal * interestRate}`);

	let dev = {
		name: "Peter",
		lastName: "Parker",
		occupation: "Web developer",
		income: 50000,
		expenses: 60000
	};

	console.log(`${dev.name} is a ${dev.occupation}.`);
	console.log(`His income is ${dev.income} and expenses at ${dev.expenses}. His current balance is ${dev.income - dev.expenses}`);

	/*
		ARRAY DESTRUCTURING
			- This allow usto unpack elements in arrays into distinct variables.

			- It also allows us to name array elements with the variables instea of using index numbers.

			- Helps with code readability.

	*/

	const fullName = ["Juan","Dela","Cruz"];

	// For arrays, use square notation []
	console.log(fullName[0]);
	console.log(fullName[1]);
	console.log(fullName[2]);
	console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);

	/*Array Destructing Syntax:
		
		Set variable for Array:
		let/const arrayName = ["variableName", "variableName", "variableName"]
		
		Destructuring Syntax:
		let/const [variableName, variableName, variableName] = arrayName


	*/

	const[firstName, middleName, lastName] = fullName;
	console.log(firstName);
	console.log(middleName);
	console.log(lastName);
	console.log(`Hello ${firstName} ${middleName} ${lastName}! I was enchanted to meet you!`);

	/*
	if we want firstName & lastName only:

	const[firstName, , lastName] = fullName;

	*/

	// Example 2:

		let fruits = ["Mango","Grapes","Guava","Apple"];
		let [fruit1, ,fruit3, ] = fruits;
		console.log(fruit1);
		console.log(fruit3);


	// Mini-activity: Array Destructuring

	let kupunanNiEugene = ["Eugene","Alfred","Vincent","Dennis","Taguro","Master Jeremiah"];

	let [name7, name2, name3, name4, ,name6] = kupunanNiEugene
	console.log(name7);
	console.log(name2);
	console.log(name3);
	console.log(name4);
	console.log(name6);

	// OR you can use:
	console.log(`${name7} ${name2} ${name3} ${name4} ${name6}`);

	/*
		OBJECT DESTRUCTURING

			- Allows us to unpack properties of objects into distinct variables.

			- Shortens the syntax for accessing properties from objects

		Syntax:
			let/const {propertyName, propertyName, propertyName} = object; 
	*/

	const person = {
		givenName: "Jane",
		maidenName: "Dela",
		familyName: "Cruz"
	}

	// Pre-Object Destructuring
	console.log(person.givenName);
	console.log(person.maidenName);
	console.log(person.familyName);
	console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again!`);


	// Object destructuring

	const {givenName, maidenName, familyName} = person;

	console.log(givenName);
	console.log(maidenName);
	console.log(familyName);

	console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you!`);

	// Object destructuring with function

	function getFullName({givenNames, maidenNames, familyNames}){
		console.log(`${givenName} ${maidenName} ${familyName}`)
	}

	getFullName(person);


	/*
		ARROW FUNCTIONS

			- compact alternative syntax to traditional functions.

			- useful for code snippets where creating functions will not be reused in any other portion of the code.

			- Adhere to the concept "DRY" (Don't Repeat Yourself) principle where there's no longer need to create a function and think of a name for functions that will only be used in a certain code snippets
	*/

	// Traditional Function
	function displayMsg(){
		console.log("Hi");
	}
	displayMsg();

	
	// Arrow Function
	let displayHello = () => {
		console.log("Hello World!")
	}
	displayHello();


	// Traditional Function
	/*
	function printFullName (firstName,middleInitial,lastName){
		console.log(`${firstName} ${middleInitial} ${lastName}`)
	}
	printFullName("John","D","Smith");

	*/


	// Arrow Function

	let printFullName = (firstName,middleInitial,lastName) => {
		console.log(`${firstName} ${middleInitial} ${lastName}`)
	}
	printFullName("John","D","Smith");


	const students = ["John","Jane","Natalia","Jobert","Joe"];

	// Traditional function for loops
	students.forEach(function(student){
		console.log(`${student} is a student!`)
	})

	// Arrow function for loops

	students.forEach((student)=>{
		console.log(`${student} is a student! (from arrow)`)
	})


/*
	IMPLICIT RETURN STATEMENT

		- There are instances when you can omit the "return" statement.

		- This works because even without the "return" statement, JS implicitly adds it for the result of the function
*/

	// Traditional function return statement
	/*
	function add (x,y){
		return x + y
	}

	let total = add(1,2);
	console.log(total);
	*/

	// Arrow function return statement:
	/*
	const add = (x,y) =>{
		return x + y
	}

	let total = add(1,2);
	console.log(total);
	*/


	// Simpler Arrow Function return statement
	const add = (x,y) => x + y
	

	let total = add(1,2);
	console.log(total);


	// DEFAULT FUNCTION ARGUMENT VALUES

		// 'User' is a default argument value

	const greet = (name = 'User') => {
		return `Good morning, ${name}!`
	}

	console.log(greet());
	console.log(greet("John"));


/*
	CLASS-BASED OBJECT BLUEPRINT - ES6 Update

		- Allows creation/instantiation of objects using classes or blueprints
*/

/*
	CREATING A CLASS

		-The constructor is a special method of a class for creating/initializing an object for that class.

		- ".this" keyword refers to the properties of an object created/initialized from the class.
*/

	class Car {
		constructor(brand,name,year){
			this.brand = brand;
			this.name = name;
			this.year = year;
		}
	}

	// to create new instance - use "new"

	let myCar = new Car();
	console.log(myCar);

	myCar.brand = "Ford";
	myCar.name = "Ranger Raptor";
	myCar.year = 2021;

	console.log(myCar);


	const myNewCar = new Car("Toyota","Vios",2021);
	console.log(myNewCar);


/*
	TRADITIONAL FUNCTIONS VS ARROW FUNCTION as Methods
*/

let character1 = {
	name:"Cloud Strife",
	occupation: "soldier",
	greet: ()=> {

		// In a traiditonal function. this keryword referes to the current where the method is
		console.log(this); 
		// global window object
			// container of browser's global variables, functions, and objects
		console.log(`Hi! I'm ${this.name}!`);
	},
	introduceJob: function(){
		console.log(`Hi! I'm ${this.name}. I'm a ${this.occupation}`)
	}
}
character1.greet()
character1.introduceJob()



	// Mini-activity:

	class Character {
		constructor(name,role,strength,weakness){
			this.name = name;
			this.role = role;
			this.strength = strength;
			this.weakness = weakness;
			this.introduce = () =>{
				console.log(`Hi! I am ${this.name}!`)
			}
		}
	} 

	const newCharacter = new Character("Mortred","Jungler","Evasion","Spell Damages");
	console.log(newCharacter);
	newCharacter.introduce();
