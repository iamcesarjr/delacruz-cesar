console.log("Hello World!");



 function addNum(num1,num2){

 	return num1 + num2;
 }

 let sum = addNum(4,10);
 console.log("Displayed sum of 4 and 10");
 console.log(sum);


 function subNum(num3,num4){

 	return num3 - num4;
 }

 let difference = subNum(25,10);
 console.log("Displayed difference of 25 and 10");
 console.log(difference);


 function multiplyNum(num5,num6){

 	return num5 * num6;
 }

 let product = multiplyNum(5,7);
 console.log("Displayed product of 5 and 7");
 console.log(product);

 function divideNum(num7,num8){

 	return num7 / num8;
 }

 let quotient = divideNum(42,6)
 console.log("Displayed quotient of  42 and 6");
 console.log(quotient);


function getCircleArea(radius){

	const pi = 3.1416;
	return pi * radius**2;
}

let circleArea = getCircleArea(6);
console.log("The result of getting the area of a circle with 6 radius");
console.log(circleArea);

 function getAverage(number1,number2,number3,number4){

 	return (number1 + number2 + number3 + number4) / 4;
 }

 let averageVar = getAverage(7,6,19,94);
 console.log("The average of 7,6,19,94:");
 console.log(averageVar);

  function checkIfPassed(pass1,pass2){
 
  	return ((pass1 / pass2)*50)
  }
 	const isPassed = 38;
  let isPassingScore = checkIfPassed(37,50) >= isPassed;
  console.log("Is 37/50 a passing score?");
  console.log(isPassingScore);

  //Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
	module.exports = {

		addNum: typeof addNum !== 'undefined' ? addNum : null,
		subNum: typeof subNum !== 'undefined' ? subNum : null,
		multiplyNum: typeof multiplyNum !== 'undefined' ? multiplyNum : null,
		divideNum: typeof divideNum !== 'undefined' ? divideNum : null,
		getCircleArea: typeof getCircleArea !== 'undefined' ? getCircleArea : null,
		getAverage: typeof getAverage !== 'undefined' ? getAverage : null,
		checkIfPassed: typeof checkIfPassed !== 'undefined' ? checkIfPassed : null,

	}
} catch(err){

}
