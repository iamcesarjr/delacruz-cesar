
	// Functions - Parameters & Arguments

	// function printName() {
	// 	let nickname = prompt("Enter your nickname:")
	// 	console.log("Hi, " + nickname);

	// }
	// printName();

	function printName(name){

		console.log("Hi, " + name);
	}
	printName("Cesar");

	let sampleVariable = "Cardo"

	printName(sampleVariable);

	function checkDivisibilityBy8(num){

		let remainder = num % 8;
		console.log("The remainder of " + num + "divided by 8 is: " + remainder)
		let isDivisibleBy8 = remainder === 0;
		console.log("Is " + num + " divisible by 8?")
		console.log(isDivisibleBy8);

	}

	checkDivisibilityBy8(64);
	checkDivisibilityBy8(28);
	checkDivisibilityBy8(9678);

	// Mini-activity

	function checkDivisibilityBy4(num){

		let remainder = num % 4;
		console.log("The remainder of " + num + " divided by 4 is: " + remainder)
		let isDivisibleBy4 = remainder === 0;
		console.log("Is " + num + " divisible by 4?")
		console.log(isDivisibleBy4);

	}

	checkDivisibilityBy4(56);
	checkDivisibilityBy4(95);
	checkDivisibilityBy4(444);



	// Functions as arguments 
	// Function parameters can also accept other functions as arguments

	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed.");
	};

	function invokeFunction(argumentFunction){
		argumentFunction();
	}

	// Adding and removing the parentheses "()" impacts the output of JS heavily 
	// When a function is used with parenthese "()", it denotes invoking/calling a function
	// A function used without a parenthesis is normally associated with using the function as an argument to another function.

	invokeFunction(argumentFunction);

	// using multiple parameters

	function createFullName(firstName, middleName, lastName){
		console.log(firstName + ' ' + middleName + ' ' + lastName);
	}

	createFullName('Juan', 'Dela', 'Cruz');
	createFullName('Cruz', 'Dela', 'Juan');
	createFullName('Juan', 'Dela');
	createFullName('Juan', 'Dela', 'Cruz', 'III');

	// using variables as arguments

	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName,middleName,lastName);


	function printFriends(friend1, friend2, friend3 ){
		console.log("My three friends are: " + friend1 + ', ' +friend2 + ', ' + friend3);
	}

	printFriends('Jessica','Reiniel', 'Mark');



	// Return Statement

	function returnFullName(firstName,middleName,lastName){

		return firstName + ' ' + middleName + ' ' + lastName;
		console.log("This will not be printed!")
	}

	let completeName1 = returnFullName("Monkey","D","Luffy");
	let completeName2 = returnFullName("Trafalgar","D","Law");

	console.log(completeName1 + " is my bestfriend!");
	console.log(completeName2 + " is my friend!");

	// Mini-ativity

	function getAreaOfSquare(side){
		return side**2;
	}
	let areaOfSquare = getAreaOfSquare(4);

	console.log("The area of the square is:")
	console.log(areaOfSquare);



	function totalSum(number1,number2,number3){
		
		return number1 + number2 + number3;
	}

	let sumOfThree = totalSum(5,6,7);
	console.log("The sum of three number is: ");
	console.log(sumOfThree);


	function checkNumber(number4){
		let placeNumber = number4 === 100;
		console.log ("Is the number equal to 100? " + placeNumber);
	}

	checkNumber(99);
	checkNumber(100);