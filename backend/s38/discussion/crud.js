let http = require("http");
let port = 4000;

let directory = [
	{
		"name":"Zara Evergreen",
		"email":"zara.evergreen@gmail.com"
	},

	{
		"name":"Joy Boy",
		"email":"joyboy@gear5.com"
	}
]

let app = http.createServer((request,response)=>{

	if (request.url == "/users" && request.method == "GET"){
		response.writeHead(200,{'Content-type':'application/json'})
		response.write(JSON.stringify(directory))
		response.end();
	}

	else if(request.url =="/users" && request.method == "POST"){
		// Route to add a new user, we have to receive an input from the client. To be able to receive the request body or the input from the request/client, we have to add a way to receive the input. In NodeJs, this is done in two steps

		// requestBody will be a placeholder to contain the data (request passed from the client)
		let requestBody = '';

		/*
			1st Step [Data Step]
				- this will read the incoming stream of data from the client and process it so we can save it in the requestBody variable.
		*/

		request.on('data',(data)=>{
			console.log(`This is the data received from the client ${data}`)
			requestBody += data;
		})

		/*
			2nd Step [End Step]
				- This will run once or after the request data has been completely sent from the client.

			Initially, requestBody is in JSON format so we cannot add this to our directory array because its a string. So we have to update requestBody variable with a parsed version of received JSON format data.
		*/

		request.on('end',()=>{

			console.log(`This is the requestBody: ${requestBody}`)
			console.log(typeof requestBody)
			requestBody = JSON.parse(requestBody)

			let newUser = {
			"name":requestBody.name,
			"email":requestBody.email
			}

			console.log(requestBody);
			console.log(typeof requestBody)
			console.log(`This is the email: ${requestBody.email}`)


			directory.push(newUser);
			console.log(`This is the email: ${newUser.email}`)

			response.writeHead(200,{'Content-type':'application/json'})
			response.write(JSON.stringify(directory))
			// response.write(JSON.stringify(newUser.name))
			response.end()
			})
		}

	})
app.listen(port,()=>console.log(`Server running at localhost:${port}`));