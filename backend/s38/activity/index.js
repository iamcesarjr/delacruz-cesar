//Add code here

let http = require("http");
let port = 4000;

let app = http.createServer((request,response)=>{

	if(request.url =="/" && request.method =="GET"){
		response.writeHead(200,{'Content-type':'application/json'});
		response.end('Welcome to booking system')
	}
	else if(request.url =="/profile" && request.method =="GET"){
		response.writeHead(200,{'Content-type':'application/json'});
		response.end('Welcome to your profile')
	}
	else if(request.url =="/courses" && request.method =="GET"){
		response.writeHead(200,{'Content-type':'application/json'});
		response.end("Here's our courses available")
	}
	else if(request.url =="/addCourses" && request.method =="POST"){
		response.writeHead(200,{'Content-type':'application/json'});
		response.end("Add course to our resources")
	}
	else if(request.url =="/updateCourses" && request.method =="PUT"){
		response.writeHead(200,{'Content-type':'application/json'});
		response.end("Update course to our resources")
	}
	else if(request.url =="/archiveCourses" && request.method =="DELETE"){
		response.writeHead(200,{'Content-type':'application/json'});
		response.end("Archive courses to our resources")
	}

})






//Do not modify
//Make sure to save the server in variable called app
if(require.main === module){
    app.listen(4000, () => console.log(`Server running at port 4000`));
}

module.exports = app;