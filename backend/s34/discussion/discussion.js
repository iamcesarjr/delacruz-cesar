/*
	C.R.U.D Operations

	 - CRUD operations are the heart of any back-end application
	 - Mastering the CRUD operations is essential for any developer.
	 - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
*/

// To switch database, we use - 'use <database Name>'

/*
[CREATE] Insert documents
	
	Insert one document (.insertOne)

	Syntax:

	db.collectionName.insertOne({object});

	collectionName - collection name in MongoDB Compass
*/
	db.users.insertOne({
		"firstName":"John",
		"lastName":"Smith"
	})

	db.users.insertOne({
		firstName:"Jane",
		lastName:"Doe",
		age:21,
		contact:{
			phone: "8700",
			email:"janedoe@gmai.com"
		},
		courses:["CSS","JavaScript","Python"],
		department:"none"
	})

/*
	Insert Many (.insertMany([]))

	Syntax:
		db.collectionName.insertMany([{objectA},{objectB}])
*/

	db.users.insertMany([

		{
			firstName:"Stephen",
			lastName:"Hawking",
			age:76,
			contact:{
				phone:"87001",
				email:"stephenhawking@gmail.com"
			},
			courses:["Python","React","PHP"],
			department:"none"
		},
		{
			"firstName":"Neil",
			lastName:"Armstrong",
			age:82,
			contact:{
				phone:"87002",
				email:"neilarmstrong@gmail.com"
			},
			courses:["React","Laravel","Sass"],
			department:"none"
		}
	])


/*
[READ] Finding Documents
	
	find()
	 - finds our documents but if multiple documents match the criteria for finding a document.

	[finding document syntax]
	
	db.collectionName.find()
	db.collectionName.find({field:value})

	
*/
	db.users.find();
	// leaving the search criteria empty will retrieve ALL the documents

	db.users.find({firstName:"Stephen"})

/*
	Finding document with multiple parameters:

	Syntax:
		db.collectionName.find({fieldA:valueA, fieldB:valueB})
*/
	db.users.find({lastName:"Armstrong",age:82})


/*
[UPDATE] Updating a document

Syntax:

	* Updating a single document:

	db.collectionName.updateOne({criteria},{$set:field,value})

	* Updating multiple documents:
	
	db.collectionName.updateMany({criteria},{$set:field,value}


*/
// let's add another document
	db.users.insertOne({
		firstName:"Test",
		lastName:"Test",
		age:0,
		contact:{
			phone:"00000",
			email:"test@gmail.com"
		},
		courses:[],
		department:"none"
	})

// Sample updating single document:
	db.users.updateOne(
		{firstName:"Test"},
		{
			$set:{
				firstName:"Bill",
				lastName:"Gates",
				age:65,
				contact:{
					phone:"12345678",
					email:"bill@gmail.com"
				},
				course:["PHP","Laravel","HTML"],
				department:"Operations",
				status:"active"
			}
		}
	)
	// status is inserted automatically

 //Sample updating multiple documents:

 	db.users.updateMany(
 		{department:"none"},
 		{
 			$set:{department:"HR"}
 		}
 	)


/*
[DELETE] Deleting documents

	* Deleting a Single Document:
	db.collectionName.deleteOne({criteria})

	* Deleting multiple document:
	db.collectionName.deleteMany({criteria}

	[WARNING]: Be careful when using the deleteMany method. If we don't add a SEARCH CRITERIA, it will DELETE ALL DOCUMENTS in a database.
	[DO NOT USE] - db.collectionName.deleteMany()


*/
// create a test document
db.users.insert({
	firstName:"test"
})

	// delete single documents
	db.users.deleteOne({
		firstName:"test"
	})

	// delete multiple documents
	db.users.deleteMany({
		firstName:"Bill"
	})

	// Replace One
/*
    - Can be used if replacing the whole document is necessary.
    - Syntax
        - db.collectionName.replaceOne( {criteria}, {$set: {field: value}});
*/

db.users.replaceOne(
    { firstName: "Bill" },
    {
        firstName: "Bill",
        lastName: "Gates",
        age: 65,
        contact: {
            phone: "12345678",
            email: "bill@gmail.com"
        },
        courses: ["PHP", "Laravel", "HTML"],
        department: "Operations"
    }
)


//[Advanced Queries]

/*
    - Retrieving data with complex data structures is also a good skill for any developer to have.
    - Real world examples of data can be as complex as having two or more layers of nested objects and arrays.
    - Learning to query these kinds of data is also essential to ensure that we are able to retrieve any information that we would need in our application
*/

// Query an embedded document
db.users.find({
    contact: {
        phone: "87654321",
        email: "stephenhawking@gmail.com"
    }
})

// Query on nested field
db.users.find(
    {"contact.email": "janedoe@gmail.com"}
)

// Querying an Array with Exact Elements
db.users.find( { courses: [ "CSS", "Javascript", "Python" ] } )

// Querying an Array without regard to order
db.users.find( { courses: { $all: ["React", "Python"] } } )
//$all operator selects the documents where the value of a field is an array that contains all the specified elements. 


// Querying an Embedded Array
//#3
db.users.insert({
    namearr: [
        {
            namea: "juan"
        },
        {
            nameb: "tamad"
        }
    ]
})

db.users.find({
    namearr: 
        {
            namea: "juan"
        }
})
