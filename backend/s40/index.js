/*
	[npm init]
		- used to initialize a new package.json file

	[package.json]
		- contains info about the project, dependencies, and other settings.

	[package-lock.json]
		- locks the version of all installed dependencies to its specific version
*/

const express = require("express")

// The 'app' below will be the server
const app = express(); 

const port = 3000;

/*
	[MIDDLEWARES]
		- a software that provides common services and capabilities to application outside of what's offered by operating system.

	app.use()
		- allows our app to read json data.
		- is a method used to run another function or method for our express.js api
		- it is used to run middlewares

	app.use(express.urlencoded({extended:true}))
		- allows our app to read data from forms
		- This also allows us to receive information on other data types such as an object which we will use throughout our application
*/
app.use(express.json())
app.use(express.urlencoded({extended:true}))

app.get("/",(req,res)=>{
	res.send("Hello World")
})

app.get("/hello",(req,res)=>{
	res.send("Hello from /hello endpoint")
})

app.post("/hello",(req,res)=>{
	res.send("Hello from the post route")
})


//MA#1 - Refactor the post route to receive data from the request body and send a message that has the data received.

app.post("/hello",(req,res)=>{
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`)
})


//MA#2
app.put("/",(req,res)=>{
	res.send("Hello from the put method route!")
})

app.delete("/",(req,res)=>{
	res.send("Hello from the delete method route!")
})


// This array will store objects when the '/signup' route is accessed

let users = []


// MA#3 - refactor signup route

app.post("/signup",(req,res)=>{
	
	console.log(req.body)


	if(req.body.username !== '' && req.body.password !== ''){

		users.push(req.body)
		res.send(`User ${req.body.username} has been successfully registered`)

	}else{
		res.send(`Please input BOTH username and password`)
	}

})




app.put("/change-password",(req,res)=>{

	let message;

	for(let i=0; i<users.length; i++){

		if(req.body.username == users[i].username){

			users[i].password = req.body.password;

			message = `User ${req.body.username}'s password has been updated!`

			console.log(users);

			break;
		}else {
			message = "User does not exist"
		}

	}
	
	res.send(message);

})

// Start of Activity:


app.get("/home",(req,res)=>{
	res.send("Welcome to the home page")
})

app.get("/users",(req,res)=>{
	res.send(users)
})

app.delete("/delete-users",(req,res)=>{

	let prompt;

	for(let i=0; i<users.length; i++){

		if(req.body.username == users[i].username){

			users.splice(i,1)
			prompt = `User ${req.body.username} has been deleted!`
		break;
		} else {
			prompt = "User does not exist"
		}
	}
	res.send(prompt);
})



// End of Activity


if(require.main === module){
	app.listen(port,()=>console.log(`Server running at port ${port}`))
}

module.exports = app;

