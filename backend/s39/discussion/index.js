/*
	JavaScript Synchronous vs. Asynchronous

	[JS by default is SYNCHRONOUS]
		- meaning that only one statement is executed at a time. If an error was encountered on a certain line/statement, the succeeding code will not be shown unless error is resolved.

	[ASYNCHRONOUS]
		- means that we can proceed to execute other statements, while time consuming code is running in the background
*/

/*
	[GETTING ALL POSTS]
		- The fetch API allows us to asynchronously request for a resource(data).

	[PROMISE]
		- an OBJECT that represents the eventual completion (or a failure) of an asynchronous function and its resulting value.
		- It is like a special container for a future value that may not be available immediately.
		- It represents the outcome of an asynch operation that may take some time to complete such as fetching data froma server, reading a file, getting data from the database and any other task that doesn't happen instantly.

	Syntax:
		fetch ('URL')
*/
	console.log(fetch('https://jsonplaceholder.typicode.com/posts'))

/*
	the 'fetch()'' function returns a 'Promise' which can then be chained using the '.then()'

	The '.then()' waits for the promise to be resolved before executing the code.

	Syntax:
		fetch('URL')
		.then((response)=>{})
*/
	fetch('https://jsonplaceholder.typicode.com/posts')
	.then(response =>console.log(response.status))

// Uset the 'json' method from the "response" object to convert the retrieved into JSON format to be used in our application
	fetch('https://jsonplaceholder.typicode.com/posts')
	.then((response)=>response.json())
	.then((posts)=>console.log(posts))
/*
	- log the converted JSON value from the 'fetch' request
	- When we have multiple '.then' methods, it creates a promise chain
*/

/*
	[ASYNC ('async') FUNCTION]
	 - waits for the "fetch" method to complete then stores th value in the "result" variable.

	 - we can't use the 'async' function without the 'await' keyword.

	[ADVANTAGE]
	One advantage po ni Async/await is it allows for writing asynchronous code in a more readable and synchronous-like manner, making it easier to understand and maintain compared to using .then for handling promises. Magiging apparent ito kung mayroon po tayong maraming fetch request sa isang function.

	On the code below, 'result' retured from fetch is a promise.
*/
	 async function fetchData(){
	 	let result = await fetch('https://jsonplaceholder.typicode.com/posts')
	 	console.log(result);
	 	console.log(typeof result);
	 	console.log(result.body);

	 	// convert the data from the response object as JSON
	 	let json = await result.json()
	 	console.log(json);
	 }
	 fetchData()

// [RETRIEVING A SPECIFIC POST]
fetch('https://jsonplaceholder.typicode.com/posts/18')
.then((response)=>response.json())
.then((json)=>console.log(json))


// [CREATING A POST]
fetch('https://jsonplaceholder.typicode.com/posts',{

	method: 'POST',
	headers: {
		'Content-type':'application/json'
	},
	body: JSON.stringify({
		title:'New Post',
		body: 'Hello World!',
		userId:1
	})
})
.then((response)=>response.json())
.then((json)=>console.log(json));


// [UPDATING A POST]
fetch('https://jsonplaceholder.typicode.com/posts/1',{

	method: 'PUT',
	headers: {
		'Content-type':'application/json'
	},
	body: JSON.stringify({
		title:'Corrected post'
	})
})
.then((response)=>response.json())
.then((json)=>console.log(json));


// [DELETING A POST]

fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method:'DELETE'
})


/*
[FILTERING A POST]
	Syntax:
		(Individual Parameters)
			'url?parameterName=value'

		(Multiple Parameters)
			'url?paramA=valueA&paramB=valueB'
*/

fetch('https://jsonplaceholder.typicode.com/posts?userId=1&id=6')
.then((response)=>response.json())
.then((json)=>console.log(json))


// [RETRIEVE NESTED/RELATED COMMENTS TO POSTS]

fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response)=>response.json())
.then((json)=>console.log(json))