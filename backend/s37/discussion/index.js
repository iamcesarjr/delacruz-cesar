let http = require("http");

/*
	We use the 'require' directive to include and load Node.js modules or packages.

	A MODULE is a software component or part of a program that contains one or more routines. These are OBJECTS that contain pre-built codes, methods, and data.

	'http' is a default module that comes from NodeJS. It allows us to transfer data using HTTP and use methods that let us create servers.

	http module lets Node.js transfer data using Hyper Text Transfer Protocol.

	HTTP is a protocol that allows the fetching of resources such as HTML documents.

	[PROTOCOL TO CLIENT-SERVER COMMUNICATION]
		http://localhost:4000 - server/application

	[CLIENT]
	 - an application which creates requests for resources from a server. A client will trigger an action in the web development context, through a URL and wait for the response of the server. 

	 [SERVER]
	 - is able to host and deliver resources that request by a client. A single server can handle multiple clients.

	 [Node.js]
	  - a runtime environment which allows us to create/develop Backend/Serve-side applications with JS. By default, JS was conceptualized solely to Front-End.

	  [RUNTIME ENVIRONMENT]
	   - the environment in which a program or application is executed.
*/

http.createServer(function(request,response){

	/*
		createServer() method is a method from the http module that allows us to handle requests and response from a client and a server respectively.

		.createServer() method takes a fucntion argument which is able to receive 2 objects:
			1. REQUEST -  object which contains details of the requests from the client.

			2. RESPONSE - object which contains details of the response from the server.

		The createServer() method ALWAYS receives the request object first before the response.
	*/

	response.writeHead(200,{'Content-type':'text/plain'})
	/*
		response.writeHead() 
		- is a method of the response object. It allows us to add headers to our response.

		1. [HTTP STATUS CODE]
			200 - means OK
			404 - means the Resource cannot be found

		2. 'Content-type'
			- pertains to the data type of our response.
	*/
	response.end('Hi, my name is Cesar!')

	/*
		response.end() 
		- ends our response. It is able to send a message/data as a string.
	*/

}).listen(4000)

/*
	.listen() - allows us to assign a port to our server. This will allow us to serve our index.js server in our local machine assigned to port 4000

	[PORT NUMBERS]:
	4000,4040,8000,5000,3000,4200
	- These are the ports usually used for testing and debugging web applications
*/

console.log('Server is running at localhost:4000')
// This will now appear in the TERMINAL - (GitBash)