/*
	Servers can actually respond differently with different requests.

	We start our request with our URL.

	A client can start a different request with a different URL.

	http://localhost:4000/
	(/) = URL endpoint

	http://localhost:4000/profile
	(/profile) = URL endpoint
*/

const http = require("http");

//creates a variable "port" to store the port number
const port = 4000;

const app = http.createServer((request,response)=>{

	/*
		Information about the URL endpoint of the request is in the request object.

		Different request, require different response.
		The process or way to respond differently to a request is called a ROUTE
	*/

	if(request.url == '/greeting'){
		response.writeHead(200, {'Content-type':'text/plain'})
		response.end('Hello, B297!')
	}
	else if(request.url == '/homepage'){
		response.writeHead(200, {'Content-type':'text/plain'})
		response.end('This is the homepage')
	}
	else {
    response.writeHead(404, { 'Content-type': 'text/plain' });
    response.end('Page Not Available');
  }

})

app.listen(port);
console.log(`Server is now accessible at localhost: ${port}`)