console.log("Intro to JSON!")

/*
	JSON
		-"JavaScript Object Notation"
		-JSON is also used in other programming languages

	SYNTAX:

		{
			"propertyA":"valueA"
			"propertyB":"valueB"
		}

	JSO are wrapped in curly braces.
	Properties/keys are wrapped in double quotations.

*/

	let sample1 = `

	{
		"name":"Jayson Derulo",
		"age":20,
		"address":{
			"city":"Quezon City",
			"country":"Philippines"
		}
	}
`;
console.log(sample1);
console.log(typeof sample1);
/*
	Q: Are we able to turn JSON into a JS Object?
	A: YES.
*/
console.log(JSON.parse(sample1));

/*
	JSON Array
	 - is an array of JSON using Array literals "[]"
*/

let sampleArr = `

	[
		{
			"email":"wackywizard@example.com",
			"password":"laughOutLoud123",
			"isAdmin": true
		},

		{
			"email":"dalisay.cardo@gmail.com ",
			"password":"alyanna4ever",
			"isAdmin": true
		},

		{
			"email":"lebroan4goat@ex.com",
			"password":"ginisamix",
			"isAdmin": false
		},

		{
			"email":"ilovejson@gmail.com",
			"password":"jsondontloveme",
			"isAdmin": false
		}

	]

`

/*
	Q: Can we use Array Methods on a JSON Array?
	A: No. Because a JSON Array is a string.

	Q: So what can we do to be able to add more items/objects into our sampleArr?
	A: PRASE the JSON array to a JS Array and save it in a variable.
*/

	let parsedSampleArr = JSON.parse(sampleArr);
	console.log(parsedSampleArr);
	console.log(typeof parsedSampleArr);


// Mini-activity: (delete the last item in the JSON array)

parsedSampleArr.pop();
console.log(parsedSampleArr);

/*
If for example, we need to send this data back to our client/front-end, it should be in JSON format.

JSON.parse() 
does not mutate or update the original JSON
We can actually turn a JS Object into a JSON again.

JSON.strigify()
this will stringify JS objects as JSON
*/

sampleArr = JSON.stringify(parsedSampleArr);
console.log(sampleArr);
console.log(typeof sampleArr);

/*
	Database (JSON) => Server/API (JSON to JS Object to process) => sent as JSON => Frontend/Client
*/

 let jsonArr =`

 		[
 			"pizza",
 			"hamburger",
 			"spaghetti",
 			"shanghai",
 			"hotdog stick on a pineapple",
 			"pancit bihon"
 		]

 `

 	/*

 	Mini-activity #2 
 	- given the json array, process it and convert it to a JS object so we  can manipulate the array

 	- delete the last item in the array and add a new item in the array.

 	-stringify the array back into JSON

 	- and update the jsonArr with the stringified array.

 	
 	*/

 	console.log(jsonArr);

 	let parsedJSONArr = JSON.parse(jsonArr);
 	console.log(parsedJSONArr);

 	parsedJSONArr.pop();
 	parsedJSONArr.push("Biko");
 	console.log(parsedJSONArr);


 	jsonArr = JSON.stringify(parsedJSONArr);
 	console.log(jsonArr);


 	// GATHER USER DETAILS

 	let firstName = prompt("What is your first name?");
 	let lastName = prompt("What is your last name?");
 	let age = prompt("What is your age?");
 	let address = {
 			city: prompt("Which city do you live in?"),
 			country: prompt("Which country does your city address belong to?")
 	};

 	let otherData = JSON.stringify({

 			firstName:firstName,
 			lastName: lastName,
 			age: age,
 			address: address
 	})

 	console.log(otherData);