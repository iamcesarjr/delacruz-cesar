console.log("Activity#1 Original Array:");

let color = ["Blue","Red","Yellow","Orange","Green","Purple"];

console.log(color);
console.log("Result:");

function colorNames() {
	let colorToString = color.join(',');

	return colorToString
}
console.log(colorNames());

console.log("Activity#2 Original Array:");
let planet = ["Venus","Mars","Earth"];
console.log(planet);

function addPlanet(){
	planet.unshift("Mercury");

	return planet;
}

console.log("Result:");
console.log(addPlanet());

console.log("Activity#3 Original Array:")
let singers = ["Ebe Dancel","Rico Blanco","Arthur Nery","Adie"];
console.log(singers);

 function replaceSinger(){
 	singers.splice(3, 1,"Franco");
 	singers.splice(singers.length + 1, 1, "Ely Buendia");

 	return singers;
 }

console.log("Result:");
console.log(replaceSinger());

