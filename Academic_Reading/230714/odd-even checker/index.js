console.log("Odd-Even Checker!");

for (let i = 1; i <= 50; i++){
	if (i % 2 === 0) {
		console.log("The number " + i + " is EVEN");
	} else {
		console.log("The number " + i + " is ODD");
	}
}