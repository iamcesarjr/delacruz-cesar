import React, { useState } from 'react';
import Swal from 'sweetalert2';

const UpdateProfile = ({updateProfileDetails}) => {
  const [profileData, setProfileData] = useState({
    firstName: '',
    lastName: '',
    mobileNo: '',
  });

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setProfileData({
      ...profileData,
      [name]: value,
    });
  };

  const handleUpdateProfile = async () => {
    try {
      const token = localStorage.getItem('token');
      const response = await fetch('http://localhost:4000/users/profile', {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(profileData),
      });

      if (response.ok) {
        Swal.fire('Success', 'Profile updated successfully!', 'success');
      const updatedDetails = await response.json();
      updateProfileDetails(updatedDetails);

      } else {
        Swal.fire('Error', 'Failed to update profile', 'error');

      }
    } catch (error) {
      console.error('Error updating profile:', error);
    }
  };

  return (
    <div className="container">
      <h1>Update Profile</h1>
      <form>
        <div className="mb-3">
          <label htmlFor="firstName" className="form-label">
            First Name
          </label>
          <input
            type="text"
            className="form-control"
            id="firstName"
            name="firstName"
            value={profileData.firstName}
            onChange={handleInputChange}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="lastName" className="form-label">
            Last Name
          </label>
          <input
            type="text"
            className="form-control"
            id="lastName"
            name="lastName"
            value={profileData.lastName}
            onChange={handleInputChange}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="mobileNo" className="form-label">
            Mobile Number
          </label>
          <input
            type="text"
            className="form-control"
            id="mobileNo"
            name="mobileNo"
            value={profileData.mobileNo}
            onChange={handleInputChange}
          />
        </div>
        <button
          type="button"
          className="btn btn-primary"
          onClick={handleUpdateProfile}
        >
          Update Profile
        </button>
      </form>
    </div>
  );
};

export default UpdateProfile;
