import { useState } from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveCourses({ course, fetchData }) {
  const [isActive, setIsActive] = useState(course.isActive);

  const archiveToggle = (e) => {
    e.preventDefault();

    const endpoint = isActive ? 'archive' : 'activate';
    fetch(`http://localhost:4000/courses/${course._id}/${endpoint}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }

      })
    
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setIsActive((prevState) => !prevState); 
        showMessage(data.isActive);
      })
  };

  const showMessage = () => {
    Swal.fire({
      title: 'Success!',
      icon: 'success',
      text: course.isActive ? 'Course Successfully Archived' : 'Course Successfully Activated',
    });
    fetchData();
  };


  return (
    <>
      <Button
        variant={isActive ? 'danger' : 'success'}
        onClick={archiveToggle}>
        {isActive ? 'Archive' : 'Activate'}
      </Button>
    </>
  );
}
