let collection = [];

// Write the queue functions below.

function print() {
    return collection;
}

function enqueue(element) {

 const addElement = new Array(collection.length + 1)
    
  for(let i=0; i < collection.length;i++){

    addElement[i] = collection[i];
  }

  addElement[collection.length] = element;

  collection.length = 0

  for(let i=0; i < addElement.length;i++){
    collection[i] = addElement[i];
  }

  return collection;

}

function dequeue() {

  for (let i = 0; i < collection.length - 1; i++) {
    collection[i] = collection[i + 1];
  }

  collection.length--;

  return collection;
    
}

function front() {
    
    return collection[0];
}

function size() {
   return collection.length
}

function isEmpty() {
    if (collection.length > 0) {
        return false
    } else {
        return true
    }

}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};